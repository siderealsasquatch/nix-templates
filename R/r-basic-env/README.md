# Basic R env

This is really just a foundation for a more comprehensive R environment. The only thing
that this adds is the `radian` interpreter for R.
