let
  pkgs = import <nixpkgs> {};

  # Get feature_engine source from PyPi and build it
  feature_engine = pkgs.python38.pkgs.buildPythonPackage rec {
    pname = "feature_engine";
    version = "1.0.2";

    src = pkgs.python38.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "1lr3zy2v3bfk5magxza4q29815v2hrq872s4cmji72kgarklxd0d";
    };

    # Feature engine dependencies
    buildInputs = with pkgs.python38Packages; [
      numpy
      pandas
      scikitlearn
      scipy
      statsmodels
    ];
  };
in
  pkgs.mkShell {
    name = "test_env_fe";
    buildInputs = with pkgs; [
      python38
      python38Packages.scipy
      python38Packages.numpy
      python38Packages.pandas
      python38Packages.scikitlearn
      python38Packages.matplotlib
      python38Packages.seaborn
      python38Packages.ipython
      feature_engine
    ];
  }
