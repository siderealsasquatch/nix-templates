{ pkgs ? import <nixpkgs> {} }:

with pkgs;
with pkgs.python38Packages;

let
  pyPkgs = python38.pkgs;

  # lineedit package build instructions
  lineedit = pyPkgs.buildPythonPackage rec {
    pname = "lineedit";
    version = "0.1.6";

    src = pyPkgs.fetchPypi {
      inherit pname version;
      sha256 = "0gvggy22s3qlz3r5lrwr5f4hzwbq7anyd2vfrzchldaf2mwm8ygl";
    };

    buildInputs = [
      pygments
      wcwidth
      six
    ];

    doCheck = false;
  };

  # rchitect package build instructions
  rchitect = pyPkgs.buildPythonPackage rec {
    pname = "rchitect";
    version = "0.3.30";

    src = pyPkgs.fetchPypi {
      inherit pname version;
      sha256 = "1bg5vrgp447czgmjjky84yqqk2mfzwwgnf0m99lqzs7jq15q8ziv";
    };

    buildInputs = [
      cffi
      six
      lineedit
    ];

    doCheck = false;
  };

  # radian package build instructions
  radian = pyPkgs.buildPythonPackage rec {
    pname = "radian";
    version = "0.5.10";

    src = pyPkgs.fetchPypi {
      inherit pname version;
      sha256 = "0plkv3qdgfxyrmg2k6c866q5p7iirm46ivhq2ixs63zc05xdbg8s";
    };

    buildInputs = [
      cffi
      wcwidth
      lineedit
      rchitect
      pygments
      six
    ];

    doCheck = false;
  };
in
mkShell {
  name = "r-basic-env";
  buildInputs = [
    # radian and associated packages
    python38
    cffi
    pygments
    six
    wcwidth
    lineedit
    rchitect
    radian

    # R and associated packages
    R
  ];
}
