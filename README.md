# `nix-shell` Environment Templates

This repository contains `shell.nix` templates for the type of work that I do (mostly data
analysis/science with Python and a smattering of R). Due to this, the packages are likely
not to be pinned to a specific version in many of the templates. You can get by using just
`nix-shell` but it's recommended to use `lorri` and `direnv` for a smoother experience.
